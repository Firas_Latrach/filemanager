import { color } from "@cloudinary/url-gen/qualifiers/background";
import { green } from "@mui/material/colors";
import "./_FileDetails.scss";

import React from "react";
import {
  Area,
  AreaChart,
  CartesianGrid,
  Cell,
  Legend,
  Pie,
  PieChart,
  ResponsiveContainer,
  Tooltip,
} from "recharts";
export default function PieChartComponent({ file }) {
  // console.log(file);
  const data = file;
  function allSize() {
    let s = 0;
    for (let i = 0; i < data.length; i++) {
      s = s + data[i].size;
    }
    return s;
  }
  allSize = allSize();
  const fileSizeInMB = (price) => {
    return (price / (1024 * 1024)).toFixed(2);
  };
  const allSizeInMb = fileSizeInMB(allSize);

  const color = [
    "#6cadc9",
    "#4557d1",
    "#232fdd",
    "#b8c6f2",
    "#b5f0f4",
    "#3b13b2",
    "#38cfe2",
  ];
  const colorNumber = (Math.random() * (3 - 1) + 1).toFixed(0);
  return (
    <div className="chart">
      <ResponsiveContainer width={400} height={300}>
        <PieChart>
          <Pie
            data={file.map((item) => ({
              name: item.name,
              fileSizeMB: item.size,
            }))}
            nameKey="name"
            dataKey="fileSizeMB"
            innerRadius={85}
            outerRadius={110}
            cx="45%"
            cy="50%"
            paddingAngle={3}
          >
            {file.map((item, index) => (
              <Cell fill={color[index]} key={item.url} />
            ))}
          </Pie>
          <text
            x={180}
            y={150}
            textAnchor="middle"
            dominantBaseline="middle"
            className="textInPin"
          >
            {allSizeInMb} MB
          </text>

          <Tooltip />
        </PieChart>
      </ResponsiveContainer>
    </div>
  );
}
