import React from "react";
import folderIcon from "../../../assets/icon-folder.svg";
import JpgIcon from "../../../assets/icon-jpg.svg";
import PdfIcon from "../../../assets/icon-pdf.svg";
import SvgIcon from "../../../assets/icon-svg.svg";
import PngIcon from "../../../assets/icon-png.svg";

import otherIcon from "../../../assets/icon-other.svg";
import mp4Icon from "../../../assets/icon-mp4.svg";
import publucIcon from "../../../assets/public-Icon.svg";
import anikaAvatar from "../../../assets/avatar-anika-visser.png";
import mironAvatar from "../../../assets/avatar-miron-vitold.png";
import { PieChart, Pie, Sector, ResponsiveContainer } from "recharts";
import PieChartComponent from "./PieChartComponent";
import { useSelector } from "react-redux";
import "./_FileDetails.scss";
export default function FileDetails() {
  const AllData = useSelector((state) => state.fileDetails.allData);

  return (
    <div>
      <div className="Hero">
        <h6>Stotage</h6>
        <span>Upgrade before reaching it</span>
      </div>
      <div>
        <PieChartComponent file={AllData} />
        <span className="underPin">
          <h3>You’ve almost reached your limit</h3> You have used 25% of your
          available storage.
        </span>
      </div>
      <div className="allCard">
        {AllData.map((file) => (
          <div className="underPinCard" key={file.url}>
            <img
              src={
                file.format === "png"
                  ? PngIcon
                  : file.format === "jpg"
                  ? JpgIcon
                  : file.format === "pdf"
                  ? PdfIcon
                  : file.format === "mp4"
                  ? mp4Icon
                  : file.format === "folder"
                  ? folderIcon
                  : otherIcon
              }
              alt=""
            />
            <div className="textContainer">
              <h6>{file.format}</h6>
              <span>{(file.size / (1024 * 1024)).toFixed(2)} MB </span>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
