import React from "react";
import ShearchBar from "./SearchBar/ShearchBar";
import CardContaine from "./CardContainer/CardContaine";
import { Pagination } from "antd";
import "./_FileContainer.scss";
export default function FileContainer() {
  return (
    <div>
      <ShearchBar />
      <CardContaine />
      <div className="Pagination">
        <Pagination defaultCurrent={1} total={1} />
      </div>
    </div>
  );
}
