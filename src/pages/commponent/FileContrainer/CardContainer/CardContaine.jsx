import React from "react";
import "./_CardContaine.scss";
import Card from "./Card";
import LefWindo from "./lefWindo";
import { useSelector } from "react-redux";
import HorizontalCard from "./HorizontalCard";

export default function CardContaine() {
  const Filedata = useSelector((state) => state.fileDetails.allData);
  const searchFilter = useSelector((state) => state.shearchContine.text);
  const cardContainerType = useSelector(
    (state) => state.CardCountinerType.text
  );
  const sortBy = useSelector((state) => state.CardCountinerType.sortBy);

  const customSort = (a, b) => {
    if (sortBy === 10) {
      return Date.parse(a.created_at) - Date.parse(b.created_at);
    } else if (sortBy === 20) {
      return Date.parse(b.created_at) - Date.parse(a.created_at);
    }
    return 0;
  };

  const sortedFiledata = [...Filedata].sort(customSort);

  // Filter the sortedFiledata array based on the search text
  const filteredFiles = sortedFiledata.filter((file) =>
    file.name.toLowerCase().includes(searchFilter.toLowerCase())
  );

  return (
    <>
      {cardContainerType !== "right" ? (
        <div className="AllCardContainer">
          {filteredFiles.map((file, index) => (
            <Card key={index} file={file} />
          ))}
          <LefWindo />
        </div>
      ) : (
        <div className="AllCardContainer">
          {filteredFiles.map((file, index) => (
            <HorizontalCard key={index} file={file} />
          ))}
          <LefWindo />
        </div>
      )}
    </>
  );
}
