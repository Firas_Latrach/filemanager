import React, { useState } from "react";
import {
  HiMiniXMark,
  HiOutlineGlobeAlt,
  HiOutlinePencil,
  HiOutlinePlusSmall,
  HiOutlineStar,
  HiOutlineTrash,
} from "react-icons/hi2";
import PngIcon from "../../../../assets/icon-png.svg";

import folderIcon from "../../../../assets/icon-folder.svg";
import JpgIcon from "../../../../assets/icon-jpg.svg";
import PdfIcon from "../../../../assets/icon-pdf.svg";
import SvgIcon from "../../../../assets/icon-svg.svg";
import otherIcon from "../../../../assets/icon-other.svg";
import mp4Icon from "../../../../assets/icon-mp4.svg";
import publucIcon from "../../../../assets/public-Icon.svg";
import anikaAvatar from "../../../../assets/avatar-anika-visser.png";
import mironAvatar from "../../../../assets/avatar-miron-vitold.png";
import "./_leftWindo.scss";
import { Space, Tag } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { handelChange } from "../../../../store/Slices/SideBarSlices";
import { AvatarGroup } from "@mui/material";
import { deleteItem } from "../../../../store/Slices/FileDetails";
const { CheckableTag } = Tag;
const tagsData = [
  "Personal",
  "Important",
  "Invoices",
  "Work",
  "Business",
  "Planning",
  "Frontend",
  "Design",
];

export default function LefWindo() {
  const [selectedTags, setSelectedTags] = useState(["Books"]);

  const handleChange = (tag, checked) => {
    const nextSelectedTags = checked
      ? [...selectedTags, tag]
      : selectedTags.filter((t) => t !== tag);
    console.log("You are interested in: ", nextSelectedTags);
    setSelectedTags(nextSelectedTags);
  };

  const change = useSelector((state) => state.sideBar.isopen);
  const file = useSelector((state) => state.sideBar.continue);

  // Data formatting function
  const dateString = file.created_at;
  const dateObject = new Date(dateString);

  function formatDate(date) {
    const options = { year: "numeric", month: "long", day: "numeric" };
    return date.toLocaleDateString(undefined, options);
  }

  const formattedDate = formatDate(dateObject);

  const fileSizeInMB = (file.size / (1024 * 1024)).toFixed(2);

  const dispatch = useDispatch();

  // Function to handle opening/closing the left window
  const hanfelChange = () => {
    dispatch(handelChange(!change));
  };

  // Function to stop propagation of clicks on certain elements
  const stopPropagation = (e) => {
    e.stopPropagation();
  };

  // Function to handle file deletion
  const handelDelet = () => {
    dispatch(deleteItem(file.url));
    hanfelChange();
  };

  return (
    <>
      {change && (
        <div className="leftWindow" onClick={stopPropagation}>
          <div className="heroIcon">
            <HiOutlineStar />
            <span onClick={hanfelChange} className="cursorPointer">
              <HiMiniXMark />
            </span>
          </div>
          <span className="hr" />
          <div className="folder_img">
            <a href={file.url} target="_blank">
              <img
                src={
                  file.format === "png"
                    ? PngIcon
                    : file.format === "jpg"
                    ? JpgIcon
                    : file.format === "pdf"
                    ? PdfIcon
                    : file.format === "mp4"
                    ? mp4Icon
                    : file.format === "folder"
                    ? folderIcon
                    : otherIcon
                }
                alt=""
              />
            </a>
          </div>
          <div className="text_container">
            <div className="Fille_name">
              <h6>{file.name}</h6>
              <HiOutlinePencil />
            </div>
            <div className="CreatedBy">
              <span className="options">Created by</span>

              <AvatarGroup max={2}>
                {file.access_mode == "public" ? (
                  <HiOutlineGlobeAlt />
                ) : (
                  <>
                    {" "}
                    <Avatar alt="Remy Sharp" src={anikaAvatar} />{" "}
                    <Avatar alt="Travis Howard" src={mironAvatar} />{" "}
                  </>
                )}
              </AvatarGroup>
            </div>
            {/* Information about the file */}
            <div className="CreatedBy" onClick={stopPropagation}>
              <span className="options">Size</span>
              <span className="options">{fileSizeInMB} Mb</span>
            </div>
            <div className="CreatedBy" onClick={stopPropagation}>
              <span className="options">Created At</span>
              <span className="options">{formattedDate}</span>
            </div>
            <div className="CreatedBy" onClick={stopPropagation}>
              <span className="options">Modified At</span>
              <span className="options"></span>
            </div>
            {/* Tags */}
            <div className="tags">
              <span className="options">Tags </span>
              <Space size={[0, 8]} wrap className="spaceTAges">
                {tagsData.map((tag) => (
                  <CheckableTag
                    key={tag}
                    checked={selectedTags.includes(tag)}
                    onChange={(checked) => handleChange(tag, checked)}
                  >
                    {tag}
                  </CheckableTag>
                ))}
              </Space>
            </div>
            {/* Shared with */}
            <div className="Shared_with" onClick={stopPropagation}>
              <span className="options">Shared With</span>
              <div className="icon_avatar_container cursorPointer">
                <HiOutlinePlusSmall className="" />
              </div>
            </div>
            {/* Actions */}
            <div className="Actions">
              <span className="options">Actions</span>
              <span onClick={handelDelet} className="cursorPointer">
                <HiOutlineTrash />
              </span>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
