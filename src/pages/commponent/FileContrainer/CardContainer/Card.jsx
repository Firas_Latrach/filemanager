import React, { useState } from "react";
import "./_CardContaine.scss";
import {
  HiOutlineEllipsisVertical,
  HiOutlineGlobeAlt,
  HiOutlineStar,
} from "react-icons/hi2";
import Avatar from "@mui/material/Avatar";
import AvatarGroup from "@mui/material/AvatarGroup";

import folderIcon from "../../../../assets/icon-folder.svg";
import JpgIcon from "../../../../assets/icon-jpg.svg";
import PdfIcon from "../../../../assets/icon-pdf.svg";
import SvgIcon from "../../../../assets/icon-svg.svg";
import PngIcon from "../../../../assets/icon-png.svg";

import otherIcon from "../../../../assets/icon-other.svg";
import mp4Icon from "../../../../assets/icon-mp4.svg";
import publucIcon from "../../../../assets/public-Icon.svg";
import anikaAvatar from "../../../../assets/avatar-anika-visser.png";
import mironAvatar from "../../../../assets/avatar-miron-vitold.png";
import LefWindo from "./lefWindo";
import { useDispatch, useSelector } from "react-redux";
import {
  addcontinue,
  handelChange,
} from "../../../../store/Slices/SideBarSlices";

export default function Card({ file }) {
  // Data formatting functions
  const dateString = file.created_at;
  const dateObject = new Date(dateString);

  function formatDate(date) {
    const options = { year: "numeric", month: "long", day: "numeric" };
    return date.toLocaleDateString(undefined, options);
  }

  // State and dispatch setup
  const fileSizeInMB = (file.size / (1024 * 1024)).toFixed(2);
  const change = useSelector((state) => state.sideBar.isopen);
  const dispatch = useDispatch();
  const [clicked, setClicked] = useState(false);

  // Function to change state when the card is clicked
  const changeState = () => {
    dispatch(handelChange(true));
    dispatch(addcontinue(file));
  };

  return (
    <div>
      <div className="Cart" onClick={() => console.log(file.url)}>
        <div className="CartHeader">
          {/* Star icon with click handler */}
          <HiOutlineStar
            className={clicked === true ? "Start react-icon " : "react-icon"}
            onClick={() => setClicked(!clicked)}
          />
          <HiOutlineEllipsisVertical />
        </div>
        <div className="filleDetails">
          {/* File icon */}
          <img
            src={
              file.format === "png"
                ? PngIcon
                : file.format === "jpg"
                ? JpgIcon
                : file.format === "pdf"
                ? PdfIcon
                : file.format === "mp4"
                ? mp4Icon
                : file.format === "folder"
                ? folderIcon
                : otherIcon
            }
            alt=""
            onClick={() => changeState()}
          />
          {/* File name with ellipsis for long names */}
          <span className="filleName">
            {file.name.length > 17 ? file.name.slice(0, 17) + "..." : file.name}
          </span>
        </div>
        <span className="hr"></span>

        <div className="lastCardSection">
          <div className="aboutFile">
            <div>
              {/* File size */}
              <span className="size">{fileSizeInMB} MB</span>
              {/* Number of items for folders */}
              {file.format === "folder" ? (
                <span className="numberOfFile"> . 17 item</span>
              ) : null}
            </div>
            {/* File creation date */}
            <div className="DataCreat"> {formatDate(dateObject)}</div>
          </div>
          <AvatarGroup max={2}>
            {file.access_mode == "public" ? (
              // Public icon for public files
              <HiOutlineGlobeAlt />
            ) : (
              <>
                {/* Avatars for other files */}
                <Avatar alt="Remy Sharp" src={anikaAvatar} />
                <Avatar alt="Travis Howard" src={mironAvatar} />
              </>
            )}
          </AvatarGroup>
        </div>
      </div>
    </div>
  );
}
