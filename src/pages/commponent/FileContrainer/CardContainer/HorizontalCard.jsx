import React from "react";
import "./HorizontalCard.scss";
import {
  HiOutlineEllipsisVertical,
  HiOutlineGlobeAlt,
  HiOutlineStar,
} from "react-icons/hi2";
import Avatar from "@mui/material/Avatar";
import AvatarGroup from "@mui/material/AvatarGroup";

import folderIcon from "../../../../assets/icon-folder.svg";
import JpgIcon from "../../../../assets/icon-jpg.svg";
import PdfIcon from "../../../../assets/icon-pdf.svg";
import SvgIcon from "../../../../assets/icon-svg.svg";
import PngIcon from "../../../../assets/icon-png.svg";

import otherIcon from "../../../../assets/icon-other.svg";
import mp4Icon from "../../../../assets/icon-mp4.svg";
import publucIcon from "../../../../assets/public-Icon.svg";
import anikaAvatar from "../../../../assets/avatar-anika-visser.png";
import mironAvatar from "../../../../assets/avatar-miron-vitold.png";
import LefWindo from "./lefWindo";
import { useDispatch, useSelector } from "react-redux";
import {
  addcontinue,
  handelChange,
} from "../../../../store/Slices/SideBarSlices";
export default function HorizontalCard({ file }) {
  const dateString = file.created_at;
  const dateObject = new Date(dateString);
  const dispatch = useDispatch();
  function formatDate(date) {
    const options = { year: "numeric", month: "long", day: "numeric" };
    return date.toLocaleDateString(undefined, options);
  }
  const changeState = () => {
    dispatch(handelChange(true));
    dispatch(addcontinue(file));
  };

  // State and dispatch setup
  const fileSizeInMB = (file.size / (1024 * 1024)).toFixed(2);
  return (
    <div>
      <div className="card">
        <div className="fileDetails">
          <img
            src={
              file.format === "png"
                ? PngIcon
                : file.format === "jpg"
                ? JpgIcon
                : file.format === "pdf"
                ? PdfIcon
                : file.format === "mp4"
                ? mp4Icon
                : file.format === "folder"
                ? folderIcon
                : otherIcon
            }
            alt=""
            onClick={() => changeState()}
          />
          <div className="Fille_name">
            <span className="text">
              {" "}
              {file.name.length > 17
                ? file.name.slice(0, 17) + "..."
                : file.name}
            </span>
            <span className="underText">
              {fileSizeInMB} MB
              {file.format === "folder" ? (
                <span className="numberOfFile"> . 17 item</span>
              ) : null}
            </span>
          </div>
        </div>

        <div className="creatAt">
          <span className="text">Created at</span>
          <span className="underText"> {formatDate(dateObject)}</span>
        </div>
        <div>
          <HiOutlineGlobeAlt />
        </div>
        <div>
          <HiOutlineStar />
        </div>
        <div>
          <HiOutlineEllipsisVertical />
        </div>
      </div>
    </div>
  );
}
