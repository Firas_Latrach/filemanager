import React from "react";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { useDispatch } from "react-redux";
import { AddSort } from "../../../../store/Slices/CardContainerType";
export default function SelectCommponent() {
  const [age, setAge] = React.useState(10);

  const handleChangeSelct = (event) => {
    setAge(event.target.value);
    dispatch(AddSort(event.target.value));
  };
  const dispatch = useDispatch();

  return (
    <div>
      <FormControl variant="filled" sx={{ m: 1, minWidth: 110 }}>
        <InputLabel id="demo-simple-select-filled-label">Sort By</InputLabel>
        <Select value={age} onChange={handleChangeSelct}>
          <MenuItem value={10} default>
            Lastest
          </MenuItem>
          <MenuItem value={20}>oldest</MenuItem>
        </Select>
      </FormControl>
    </div>
  );
}
