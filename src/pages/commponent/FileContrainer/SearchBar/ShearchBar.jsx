import React from "react";
import { HiMiniMagnifyingGlass } from "react-icons/hi2";
import ViewModuleIcon from "@mui/icons-material/ViewModule";
import ToggleButton from "@mui/material/ToggleButton";
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup";
import FormatAlignRightIcon from "@mui/icons-material/FormatAlignRight";
import SelectCommponent from "./Select";
import "./_ShearchBar.scss";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { addContaxt } from "../../../../store/Slices/ShearchInput";
import { Addtext } from "../../../../store/Slices/CardContainerType";
import Lottie from "lottie-react";
import searchIcon from "../../../../assets/animation_lnj03zfz.json";
export default function ShearchBar() {
  const [view, setView] = React.useState("list");
  const dispatch = useDispatch();
  const handleChange = (event, nextView) => {
    setView(nextView);
    dispatch(Addtext(nextView));
  };
  const handelShearch = (e) => {
    dispatch(addContaxt(e.target.value));
  };
  return (
    <div className="shearch_bar">
      <div className="shearch_input">
        <Lottie animationData={searchIcon} className="shearchIcon" />

        {/* <HiMiniMagnifyingGlass className="reacr-icon" /> */}
        <input
          type="text"
          name=""
          id=""
          placeholder="Search"
          onChange={handelShearch}
        />
      </div>
      <div className="buttonGroup">
        <ToggleButtonGroup value={view} exclusive onChange={handleChange}>
          <ToggleButton value="module" aria-label="module">
            <ViewModuleIcon />
          </ToggleButton>
          <ToggleButton value="right" key="right">
            <FormatAlignRightIcon />
          </ToggleButton>
        </ToggleButtonGroup>
      </div>

      <div className="selectCommponenet">
        <SelectCommponent />
      </div>
    </div>
  );
}
