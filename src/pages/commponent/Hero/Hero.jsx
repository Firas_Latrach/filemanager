import React, { useState } from "react";
import { HiArrowUpTray } from "react-icons/hi2";
import "../Hero/_Hero.scss";
import { Button, Modal } from "antd";
import UplodFile from "./UplodFile";
import getUploadedFiles from "./GetFile";

import uplodIcon from "../../../assets/animation_lnizx20w.json";
import Lottie from "lottie-react";
export default function Hero() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  // console.log(getUploadedFiles);
  return (
    <div className="Hero_Container">
      <h2> File Manager</h2>
      <div className="Button" onClick={() => setIsModalOpen((show) => !show)}>
        <Lottie animationData={uplodIcon} className="uplod" />
        Upload
      </div>
      <Modal
        title="Upload Files"
        open={isModalOpen}
        onOk={handleOk}
        centered
        onCancel={handleCancel}
      >
        <UplodFile />
      </Modal>
    </div>
  );
}
