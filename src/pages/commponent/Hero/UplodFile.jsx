import React, { useState, useEffect } from "react";
import axios from "axios";
import { HiArrowUpTray } from "react-icons/hi2";
import "./UploadFile.scss";
import { message } from "antd";
import { useDispatch } from "react-redux";
import { addItem } from "../../../store/Slices/FileDetails";
import uplodIcon from "../../../assets/animation_lnizfha7.json";

import Lottie from "lottie-react";

export default function UploadFile() {
  const [file, setFile] = useState(null);
  const [url, setUrl] = useState(null);
  const [uploadedFiles, setUploadedFiles] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    // Load uploadedFiles from local storage when the component mounts
    const storedFiles = localStorage.getItem("uploadedFiles");
    if (storedFiles) {
      setUploadedFiles(JSON.parse(storedFiles));
    }
  }, []);

  const uploadFiles = async () => {
    if (!file) {
      message.error("Please select a file to upload.");
      return;
    }

    const form = new FormData();
    form.append("file", file);
    form.append("upload_preset", "firaslatrach");

    try {
      const response = await axios.post(
        "https://api.cloudinary.com/v1_1/dm5d9jmf4/upload",
        form
      );

      // Create an object with uploaded file data
      const uploadedFileData = {
        url: response.data.secure_url,
        format: response.data.format,
        name: file.name,
        size: file.size,
        asset_id: response.data.asset_id,
        created_at: response.data.created_at,
        access_mode: response.data.access_mode,
      };

      // Update uploadedFiles state
      const updatedFiles = [...uploadedFiles, uploadedFileData];
      setUploadedFiles(updatedFiles);

      // jlhjvljb;

      // Save uploadedFiles to local storage
      localStorage.setItem("uploadedFiles", JSON.stringify(updatedFiles));

      // Dispatch the addItem action with the uploaded file data
      dispatch(addItem(uploadedFileData));

      setUrl(response.data.secure_url);
      message.success(`${file.name} file uploaded successfully.`);
      setFile(null);
    } catch (error) {
      console.error("Error uploading file:", error);
      message.error(`File upload failed.`);
      setUrl(null);
    }
  };

  return (
    <div className="upload-file-container">
      <label className="file-upload-label">
        <div className="upload-icon">
          <Lottie animationData={uplodIcon} />
          {/* <HiArrowUpTray className="icon" /> */}
        </div>
        <input
          className="file-input"
          type="file"
          accept=".jpg, .jpeg, .png, .gif , .rar ,.zip  ,.pdf ,.svg"
          onChange={(e) => setFile(e.target.files[0])}
        />
        <span className="file-upload-text">
          Upload File Max File Size is 3MP
        </span>
      </label>
      {url && <img className="uploaded-image" src={url} alt="Uploaded" />}
      <button className="upload-button" onClick={uploadFiles}>
        {url != null ? "Add One " : "Upload"}
      </button>

      {/* Display the uploaded files */}
      <div className="uploaded-files">
        <h3>Uploaded Files:</h3>
        <ul>
          {uploadedFiles.map((item, index) => (
            <li key={index}>
              <a href={item.url} target="_blank" rel="noopener noreferrer">
                {item.name} ({(item.size / 1024).toFixed(2)} KB)
              </a>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}
