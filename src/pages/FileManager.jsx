import React from "react";
import "./_FileManager.scss";
import Hero from "./commponent/Hero/Hero";
import FileContainer from "./commponent/FileContrainer/FileContainer";
import FileDetails from "./commponent/FileDetails/FileDetails";
export default function FileManager() {
  return (
    <div className="FileManagerLayout">
      <div className="Hero">
        <Hero />
      </div>
      <main>
        <span className="FileContainer">
          <FileContainer />
        </span>
        <span className="FileDetails">
          <FileDetails />
        </span>
      </main>
    </div>
  );
}
