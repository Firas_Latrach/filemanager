import { configureStore } from "@reduxjs/toolkit";
import siderBarReducer from "./Slices/SideBarSlices";
import fileDetailsReducer from "./Slices/FileDetails";
import ShearchContineReducer from "./Slices/ShearchInput";
import CardCountinerTypeReducer from "./Slices/CardContainerType";
// Create a Redux store by combining reducers
const store = configureStore({
  reducer: {
    // Add reducers for different parts of the store
    sideBar: siderBarReducer,
    fileDetails: fileDetailsReducer,
    shearchContine: ShearchContineReducer,
    CardCountinerType: CardCountinerTypeReducer,
  },
});

// Export the created Redux store
export default store;
