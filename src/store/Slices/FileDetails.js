import { createSlice } from "@reduxjs/toolkit";

const FileDetails = createSlice({
  name: "fileDetails",
  initialState: {
    data: [], // Current state data
    allData: JSON.parse(localStorage.getItem("uploadedFiles")) || [], // Initial state loaded from localStorage
  },
  reducers: {
    addItem(state, action) {
      // Add the new item to the state and update localStorage
      state.allData.push(action.payload);
      localStorage.setItem("uploadedFiles", JSON.stringify(state.allData));
    },
    deleteItem(state, action) {
      // Remove the item with the matching URL from the state and update localStorage
      state.allData = state.allData.filter(
        (item) => item.url !== action.payload
      );
      localStorage.setItem("uploadedFiles", JSON.stringify(state.allData));
    },
  },
});

export const { addItem, deleteItem } = FileDetails.actions;

export default FileDetails.reducer;
