import { createSlice } from "@reduxjs/toolkit";

const ShearchContine = createSlice({
  name: "shearchContine",
  initialState: {
    text: "",
  },
  reducers: {
    addContaxt(state, action) {
      state.text = action.payload;
    },
  },
});

export const { addContaxt } = ShearchContine.actions;

export default ShearchContine.reducer;
