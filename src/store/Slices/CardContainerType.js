import { createSlice } from "@reduxjs/toolkit";

const CardCountinerType = createSlice({
  name: "CardCountinerType",

  initialState: {
    text: "",
    sortBy: 10,
  },
  reducers: {
    Addtext(state, action) {
      state.text = action.payload;
    },
    AddSort(state, action) {
      state.sortBy = action.payload;
    },
  },
});

export const { Addtext, AddSort } = CardCountinerType.actions;

export default CardCountinerType.reducer;
