import { Outlet } from "react-router-dom";
import Header from "./Header";
import SideBar from "./SideBar";
import "./_AppLayout.scss";
import { useDispatch, useSelector } from "react-redux";
import { handelChange } from "../store/Slices/SideBarSlices";
import { useRef } from "react";
import { CloseFullscreen } from "@mui/icons-material";

const AppLayout = () => {
  const isSidebarOpen = useSelector((state) => state.sideBar.isopen);
  const dispatch = useDispatch();

  const toggleSidebar = () => {
    dispatch(handelChange(!isSidebarOpen));
  };

  // Create a ref for the main content area
  const mainRef = useRef();

  return (
    <div className="AppLayout">
      <Header />
      <SideBar />
      <main
        ref={mainRef}
        className={`dk ${isSidebarOpen ? "sidebar-open" : ""}`}
        onClick={() => {
          if (isSidebarOpen) {
            toggleSidebar();
          }
        }}
      >
        <Outlet />
      </main>
    </div>
  );
};

export default AppLayout;
