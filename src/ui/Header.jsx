import "./_Header.scss";
import { HiOutlineBars3 } from "react-icons/hi2";
import { HiMiniMagnifyingGlass } from "react-icons/hi2";
import { HiOutlineBell } from "react-icons/hi2";
import { HiOutlineUsers } from "react-icons/hi2";
import { NavLink } from "react-router-dom";
import defaultuser from "../assets/default-use.png";
import { useDispatch, useSelector } from "react-redux";
import { handelChange } from "../store/Slices/SideBarSlices";
import tunisiFlag from "../assets/tunisia-flag-icon-16.png";
import { Tooltip } from "@mui/material";
import tunisiFlagAnimatio from "../assets/animation_lnizrxje.json";
import Lottie from "lottie-react";

export default function Header() {
  // const dispatch = useDispatch();
  // // const isopen = useSelector((stat) => stat.sideBar.isopen);

  // }
  return (
    <div className="header">
      <div className="right__header-section">
        {/* <HiOutlineBars3 className="burger-menu" /> */}
        <Tooltip title="search" followCursor className="search">
          <span>
            <HiMiniMagnifyingGlass className="reacr-icon" />
          </span>
        </Tooltip>
      </div>
      <div className="left__header-section">
        {/* <span className="fullName">Firas Latrach</span> */}
        <Tooltip title="Language" followCursor className="tunisiFlag">
          <span>
            {" "}
            <Lottie animationData={tunisiFlagAnimatio} />
          </span>
        </Tooltip>

        <Tooltip title="Notifications" followCursor>
          <div className="notifcation">
            <div className="badge">5</div>
            <HiOutlineBell className="reacr-icon" />
          </div>
        </Tooltip>

        <Tooltip title="Contacts" followCursor className="contact">
          <div>
            <HiOutlineUsers className="reacr-icon" />
          </div>
        </Tooltip>

        <img src={defaultuser} alt="" className="userImage" />
      </div>
    </div>
  );
}
