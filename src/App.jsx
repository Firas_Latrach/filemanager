import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import "./styles/style.scss";
import FileManager from "./pages/FileManager";
import AppLayout from "./ui/AppLayout";
function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route element={<AppLayout />}>
          <Route index element={<Navigate replace to="filemanager" />} />

          <Route path="filemanager" element={<FileManager />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
